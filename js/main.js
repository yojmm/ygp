var actions = {
	suit_up: {
		label: 'Suit Up!',
		function: function() {
			alert('azaz');
		}
	},
	kick_ass: {
		label: 'Kick Ass!',
		function: function() {
			alert('ololo');
		}
	}
}

var locations = {
	location1: {
		label: 'Lokaciya 1',
		description: 'Kakayato-lokciya 1',
		routes: [
			'location2',
			'location3',
			'location4'
		],
		actions: [
			'suit_up',
			'kick_ass'
		]
	},
	location2: {
		label: 'Lokaciya 2',
		description: 'Kakayato-lokciya 2',
		routes: [
			'location1',
			'location5',
			'location6'
		]
	},
	location3: {
		label: 'Lokaciya 3',
		description: 'Kakayato-lokciya 3',
		routes: [
			'location1',
			'location7'
		]
	},
	location4: {
		label: 'Lokaciya 4',
		description: 'Kakayato-lokciya 4',
		routes: [
			'location1',
			'location5'
		]
	},
	location5: {
		label: 'Lokaciya 5',
		description: 'Kakayato-lokciya 5',
		routes: [
			'location2',
			'location4'
		]
	},
	location6: {
		label: 'Lokaciya 6',
		description: 'Kakayato-lokciya 6',
		routes: [
			'location2'
		]
	},
	location7: {
		label: 'Lokaciya 7',
		description: 'Kakayato-lokciya 7',
		routes: [
			'location3'
		]
	}
}

function do_action(action) {

	var action_info = actions[action];

	action_info.function();

}

/* basic function for moving between locations */

function goto_location(location) {

	/* getting the information about the location */

	var location_info = locations[location];

	/* setting the location name and desciption into the appropriate fields */

	jQuery('.location_name').text(location_info.label);

	jQuery('.location_description').text(location_info.description);

	/* clearing any old availiable location routes */

	jQuery('.location_routes').empty();

	/* getting all the availible routes for the current location */

	var location_routes = location_info.routes;

	/* looping through all of the availible routes for the current location */

	for (location_key = 0; location_key < location_routes.length; location_key++) {

		/* adding the route buttons in the appropriate container */

		jQuery('.location_routes').append('<button class="goto_location" data-location-id="' + location_routes[location_key] + '">' + location_routes[location_key] + '</button>');

	}

	/* clearing any old active location actions */

	jQuery('.location_actions').empty();

	/* getting all the availible actions for the current location */

	var location_actions = location_info.actions;

	/* looping through all of the availible actions for the current location */

	for (action_key = 0; action_key < location_actions.length; action_key++) {

		/* getting the action id from the location actions list */

		var action_id = location_actions[action_key];

		/* getting the action object by its id */

		var action = actions[action_id];

		/* getting the action label */

		var action_label = action.label;

		/* adding the action buttons in the appropriate container */

		jQuery('.location_actions').append('<button class="do_action" data-action-id="' + location_actions[action_key] + '">' + action_label + '</button>');

	}

}

jQuery(document).on('click', 'button.goto_location', function() {

	var location = jQuery(this).data('location-id');

	goto_location(location);

})

jQuery(document).on('click', 'button.do_action', function() {

	var action = jQuery(this).data('action-id');

	do_action(action);

})

jQuery(document).ready(function(){

	goto_location('location1');

	

});